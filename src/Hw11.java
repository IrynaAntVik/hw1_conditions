public class Hw11 {
    public String tochka(double x, double y) {
        if (x == 0) {
            throw new IllegalArgumentException("х не должен быть равен нулю");
            }
        if (y == 0) {
            throw new IllegalArgumentException("y не должен быть равен нулю");
        }
        if (x > 0 & y > 0) {
            return "Tochka prinadlegit 1-i chetverti";
        } else if (x > 0) {
            return "Tochka prinadlegit 4-i chetverti";
        } else if (x < 0 & y > 0) {
            return "Tochka prinadlegit 2-i chetverti";
        } else {
            return "Tochka prinadlegit 3-i chetverti";
        }
    }
}
