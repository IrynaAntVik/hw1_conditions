public class Hw13 {

    public double slogenie(double a, double b, double c) {
        double res = 0;
        if (a > 0) {
            res += a;
        }
        if (b > 0) {
            res += b;
        }
        if (c > 0) {
            res += c;
        }
        return res;
    }
}