public class Hw15 {
    public String ocenka(int reiting) {
        if (reiting < 0) {
            throw new IllegalArgumentException("Рейтинг не может быть меньше нуля");
        }
        if (reiting > 100) {
            throw new IllegalArgumentException("Рейтинг не можент быть больше 100");
        }
        if (reiting >= 90) {
            return "Оценка А";
        } else if (reiting >= 75) {
            return "Оценка B";
        } else if (reiting >= 60) {
            return "Оценка C";
        } else if (reiting >= 40) {
            return "Оценка D";
        } else if (reiting >= 20) {
            return "Оценка E";
        } else {
            return "Оценка F";
        }
    }
}
