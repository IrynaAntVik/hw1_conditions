import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestHw1 {
    private Hw11 hw11;
    private Hw12 hw12;
    private Hw13 hw13;
    private Hw14 hw14;
    private Hw15 hw15;

    @Before
    public void setupUp(){
        hw11 = new Hw11();
        hw12 = new Hw12();
        hw13 = new Hw13();
        hw14 = new Hw14();
        hw15 = new Hw15();
    }

    @After
    public void setupDown(){
        hw11 = null;
        hw12 = null;
        hw13 = null;
        hw14 = null;
        hw15 = null;
    }

    @Test(expected = IllegalArgumentException.class)
    public void HW11_Tochka_0_0(){
        hw11.tochka(0,0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void HW11_Tochka_1_0(){
        hw11.tochka(1,0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void HW11_Tochka_0_1(){
        hw11.tochka(0,1);
    }

    @Test
    public void HW11_CoordPlane_1(){
        String res = hw11.tochka(1,8);
        Assert.assertEquals("Tochka prinadlegit 1-i chetverti",res);
    }

    @Test
    public void HW11_CoordPlane_2(){
        String res = hw11.tochka(-1,8);
        Assert.assertEquals("Tochka prinadlegit 2-i chetverti",res);
    }

    @Test
    public void HW11_CoordPlane_3(){
        String res = hw11.tochka(-1,-8);
        Assert.assertEquals("Tochka prinadlegit 3-i chetverti",res);
    }

    @Test
    public void HW11_CoordPlane_4(){
        String res = hw11.tochka(1,-8);
        Assert.assertEquals("Tochka prinadlegit 4-i chetverti",res);
    }

    @Test
    public void HW12_ChetnoeChet(){
        int res = hw12.chetnoe(8, 3);
        Assert.assertEquals(24,res);
    }

    @Test
    public void HW12_ChetnoeNechet(){
        int res = hw12.chetnoe(7, 3);
        Assert.assertEquals(10,res);
    }

    @Test
    public void HW13_Sum_1_1_1(){
        double res = hw13.slogenie(1, 1, 1);
     Assert.assertEquals(3.0, res, 0);
    }

    @Test
    public void HW13_Sum_1_1_otr(){
        double res = hw13.slogenie(1, 1, -1);
        Assert.assertEquals(2.0, res, 0);
    }

    @Test
    public void HW13_Sum_1_otr_otr(){
        double res = hw13.slogenie(1, -1, -1);
        Assert.assertEquals(1.0, res, 0);
    }

    @Test
    public void HW13_Sum_otr_otr_otr(){
        double res = hw13.slogenie(-1, -1, -1);
        Assert.assertEquals(0.0, res, 0);
    }

    @Test
    public void HW13_Sum_otr_1_1(){
        double res = hw13.slogenie(-1, 1, 1);
        Assert.assertEquals(2.0, res, 0);
    }

    @Test
    public void HW13_Sum_otr_1_otr(){
        double res = hw13.slogenie(-1, 1, -1);
        Assert.assertEquals(1.0, res, 0);
    }

    @Test
    public void HW13_Sum_otr_otr_1(){
        double res = hw13.slogenie(-1, -1, 1);
        Assert.assertEquals(1.0, res, 0);
    }

    @Test
    public void HW13_Sum_1_otr_1(){
        double res = hw13.slogenie(1, -1, 1);
        Assert.assertEquals(2.0, res, 0);
    }

    @Test
    public void HW14_Max_Mult(){
        double res = hw14.max(3, 17, 38);
        Assert.assertEquals(1941.0, res, 0);
    }

    @Test
    public void HW14_Max_Sum(){
        double res = hw14.max(1, 1, 1);
        Assert.assertEquals(6.0, res, 0);
    }

    @Test
    public void HW14_Max_Equally(){
        double res = hw14.max(1, 2, 3);
        Assert.assertEquals(9.0, res, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void HW15_Reiting_Otr(){ hw15.ocenka(-40);}

    @Test(expected = IllegalArgumentException.class)
    public void HW15_Reiting_Over(){ hw15.ocenka(105);}

    @Test
    public void HW15_Ocenka_A() {
        String res = hw15.ocenka(95);
        Assert.assertEquals("Оценка А", res);
    }

    @Test
    public void HW15_Ocenka_B() {
        String res = hw15.ocenka(85);
        Assert.assertEquals("Оценка B", res);
    }

    @Test
    public void HW15_Ocenka_C() {
        String res = hw15.ocenka(65);
        Assert.assertEquals("Оценка C", res);
    }

    @Test
    public void HW15_Ocenka_D() {
        String res = hw15.ocenka(45);
        Assert.assertEquals("Оценка D", res);
    }

    @Test
    public void HW15_Ocenka_E() {
        String res = hw15.ocenka(25);
        Assert.assertEquals("Оценка E", res);
    }

    @Test
    public void HW15_Ocenka_F() {
        String res = hw15.ocenka(5);
        Assert.assertEquals("Оценка F", res);
    }
}